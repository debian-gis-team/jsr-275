/*
 * JScience - Java Tools and Libraries for the Advancement of Sciences
 * Copyright (c) 2005-2009, JScience (http://jscience.org/)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package javax.measure.converter;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * <p> This class represents a linear converter. A converter is linear if
 * <code>convert(u + v) == convert(u) + convert(v)</code> and
 * <code>convert(r * u) == r * convert(u)</code>. For linear converters the
 * following property always hold:[code]
 *     y1 = c1.convert(x1); 
 *     y2 = c2.convert(x2); 
 *     // then y1*y2 == c1.concatenate(c2).convert(x1*x2)
 * [/code] </p>
 * 
 * <p> {@link LinearConverter#concatenate Concatenation} of linear converters 
 *     always result into a linear converter.</p>
 * 
 * @author <a href="mailto:jean-marie@dautelle.com">Jean-Marie Dautelle</a>
 * @version 1.0, April 15, 2009
 */
public abstract class LinearConverter extends UnitConverter {

	@Override
	public UnitConverter concatenate(UnitConverter converter) {
		if (converter == IDENTITY)
			return this;
		if (!(converter instanceof LinearConverter))
			return super.concatenate(converter); // Compound non-linear
													// converter.
		return new CompoundLinear(this, (LinearConverter) converter);
	}

	@Override
	public abstract LinearConverter inverse(); // Inverse of a linear converter
												// should be linear.

	/**
	 * This inner class represents a compound linear converter.
	 */
	private static class CompoundLinear extends LinearConverter {

		/**
		 * Holds the first converter.
		 */
		private final LinearConverter _first;
		/**
		 * Holds the second converter.
		 */
		private final LinearConverter _second;

		/**
		 * Creates a compound linear converter resulting from the combined
		 * transformation of the specified converters.
		 * 
		 * @param first the first converter.
		 * @param second the second converter.
		 */
		private CompoundLinear(LinearConverter first, LinearConverter second) {
			_first = first;
			_second = second;
		}

		@Override
		public LinearConverter inverse() {
			return new CompoundLinear(_second.inverse(), _first.inverse());
		}

		@Override
		public double convert(double value) {
			return _second.convert(_first.convert(value));
		}

		@Override
		public BigDecimal convert(BigDecimal value, MathContext ctx) {
			return _second.convert(_first.convert(value, ctx), ctx);
		}

		@Override
		public boolean equals(Object cvtr) {
			if (this == cvtr)
				return true;
			if (!(cvtr instanceof CompoundLinear))
				return false;
			CompoundLinear that = (CompoundLinear) cvtr;
			return (this._first.equals(that._first))
					&& (this._second.equals(that._second));
		}

		@Override
		public int hashCode() {
			return _first.hashCode() + _second.hashCode();
		}

		private static final long serialVersionUID = 1L;
	}

	private static final long serialVersionUID = 1L;
}