/*
 * JScience - Java Tools and Libraries for the Advancement of Sciences
 * Copyright (c) 2005-2009, JScience (http://jscience.org/)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package javax.measure.converter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;

/**
 * <p> This class represents a converter of numeric values.</p>
 * 
 * <p> It is not required for sub-classes to be immutable
 *     (e.g. currency converter).</p>
 *     
 * <p> Sub-classes must ensure unicity of the {@link #IDENTITY identity} 
 *     converter. In other words, if the result of an operation is equivalent
 *     to the identity converter, then the unique {@link #IDENTITY} instance 
 *     should be returned.</p>
 *
 * @author  <a href="mailto:jean-marie@dautelle.com">Jean-Marie Dautelle</a>
 * @version 1.0, April 15, 2009
 */
public abstract class UnitConverter implements Serializable {

    /**
     * Holds the identity converter (unique). This converter does nothing
     * (<code>ONE.convert(x) == x</code>). This instance is unique.
     * (
     */
    public static final UnitConverter IDENTITY = new Identity();

    /**
     * Default constructor.
     */
    protected UnitConverter() {
    }

    /**
     * Returns the inverse of this converter. If <code>x</code> is a valid
     * value, then <code>x == inverse().convert(convert(x))</code> to within
     * the accuracy of computer arithmetic.
     *
     * @return the inverse of this converter.
     */
    public abstract UnitConverter inverse();

    /**
     * Converts a <code>double</code> value. 
     *
     * @param  value the numeric value to convert.
     * @return the <code>double</code> value after conversion.
     */
    public abstract double convert(double value);

    /**
     * Converts a {@link BigDecimal} value.
     *
     * @param value the numeric value to convert.
     * @param ctx the math context being used for conversion.
     * @return the decimal value after conversion.
     * @throws ArithmeticException if the result is inexact but the
     *         rounding mode is <code>MathContext.UNNECESSARY</code> or
     *         <code>mathContext.precision == 0</code> and the quotient has a
     *         non-terminating decimal expansion.
     */
    public abstract BigDecimal convert(BigDecimal value,  MathContext ctx) throws ArithmeticException;

    /**
     * Indicates whether this converter is considered to be the the same as the
     * one specified.
     *
     * @param  cvtr the converter with which to compare.
     * @return <code>true</code> if the specified object is a converter 
     *         considered equals to this converter;<code>false</code> otherwise.
     */
    @Override
    public abstract boolean equals(Object cvtr);

    /**
     * Returns a hash code value for this converter. Equals object have equal
     * hash codes.
     *
     * @return this converter hash code value.
     * @see    #equals
     */
    @Override
    public abstract int hashCode();

    /**
     * Concatenates this converter with another converter. The resulting
     * converter is equivalent to first converting by the specified converter,
     * and then converting by this converter. 
     * 
     * <p>Note: Implementations must ensure that the {@link #IDENTITY} instance
     *          is returned if the resulting converter is an identity 
     *          converter.</p> 
     * 
     * @param  converter the other converter.
     * @return the concatenation of this converter with the other converter.
     */
    public UnitConverter concatenate(UnitConverter converter) {
        return (converter == IDENTITY) ? this : new Compound(converter, this);
    }

    /**
     * This inner class represents the identity converter (singleton).
     */
    private static final class Identity extends LinearConverter {

        @Override
        public Identity inverse() {
            return this;
        }

        @Override
        public double convert(double value) {
            return value;
        }

        @Override
        public BigDecimal convert(BigDecimal value, MathContext ctx) {
            return value;
        }

        @Override
        public UnitConverter concatenate(UnitConverter converter) {
            return converter;
        }

        @Override
        public boolean equals(Object cvtr) {
            return this == cvtr; // Unique instance.
        }

        @Override
        public int hashCode() {
            return 0;
        }

        private static final long serialVersionUID = 1L;

    }

    /**
     * This inner class represents a compound converter (non-linear).
     */
    private static final class Compound extends UnitConverter {

        /**
         * Holds the first converter.
         */
        private final UnitConverter _first;
        /**
         * Holds the second converter.
         */
        private final UnitConverter _second;

        /**
         * Creates a compound converter resulting from the combined
         * transformation of the specified converters.
         *
         * @param  first the first converter.
         * @param  second the second converter.
         */
        private Compound(UnitConverter first, UnitConverter second) {
            _first = first;
            _second = second;
        }

        @Override
        public UnitConverter inverse() {
            return new Compound(_second.inverse(), _first.inverse());
        }

        @Override
        public double convert(double value) {
            return _second.convert(_first.convert(value));
        }

        @Override
        public BigDecimal convert(BigDecimal value, MathContext ctx) {
            return _second.convert(_first.convert(value, ctx), ctx);
        }

        @Override
        public boolean equals(Object cvtr) {
            if (this == cvtr)
                return true;
            if (!(cvtr instanceof Compound)) 
                return false;            
            Compound that = (Compound) cvtr;
            return (this._first.equals(that._first)) &&
                    (this._second.equals(that._second));
        }

        @Override
        public int hashCode() {
            return _first.hashCode() + _second.hashCode();
        }

        private static final long serialVersionUID = 1L;
    }
    private static final long serialVersionUID = 1L;

}