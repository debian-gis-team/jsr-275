/*
 * JScience - Java Tools and Libraries for the Advancement of Sciences
 * Copyright (c) 2005-2009, JScience (http://jscience.org/)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package javax.measure;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;

import java.text.ParsePosition;
import javax.measure.converter.UnitConverter;
import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;

/**
 * <p> This class represents the immutable result of a scalar measurement stated
 *     in a known unit.</p>
 * 
 * <p> To avoid any lost of precision, known exact measure (e.g. physical 
 *     constants) should not be created from <code>double</code> constants but
 *     from their decimal representation.[code]
 *         public static final Measure<Velocity> C = Measure.valueOf("299792458 m/s", Velocity.class); // Speed of Light (exact).
 *    [/code]</p>
 * 
 * <p> Measures can be converted to different units, the conversion precision is
 *     determined by the specified {@link MathContext}.[code]
 *         Measure<Velocity> milesPerHour = C.to(MILES_PER_HOUR, MathContext.DECIMAL128); // Use BigDecimal implementation.
 *         System.out.println(milesPerHour);
 * 
 *         > 670616629.3843951324266284896206156 [mi_i]/h
 *     [/code]
 *     If no precision is specified <code>double</code> precision is assumed.[code]
 *         Measure<Velocity> milesPerHour = C.to(MILES_PER_HOUR); // Use double implementation (fast).
 *         System.out.println(milesPerHour);
 * 
 *         > 670616629.3843951 [mi_i]/h
 *     [/code]</p>
 * 
 * <p> Applications may sub-class {@link Measure} for particular measurements
 *     types.[code]
 *         // Measurement of type Mass based on <code>double</code> primitive types.
 *         public class Weight extends Measure<Mass> { 
 *             private final double _kilograms; // Internal SI representation. 
 *             private Weight(double kilograms) { _kilograms = kilograms; }
 *             public static Weight valueOf(double value, Unit<Mass> unit) {
 *                 return new Weight(unit.getConverterTo(SI.KILOGRAM).convert(value));
 *             } 
 *             public Unit<Mass> getUnit() { return SI.KILOGRAM; } 
 *             public Double getValue() { return _kilograms; } 
 *             ...
 *         }
 * 
 *         // Complex numbers measurements.
 *         public class ComplexMeasure<Q extends Quantity> extends Measure<Q> {
 *             public Complex getValue() { ... } // Assuming Complex is a Number.
 *             ... 
 *         }
 * 
 *         // Specializations of complex numbers measurements.
 *         public class Current extends ComplexMeasure<ElectricCurrent> {...} 
 *         public class Tension extends ComplexMeasure<ElectricPotential> {...}
 *         [/code]</p>
 * 
 * <p> All instances of this class shall be immutable.</p>
 * 
 * @author <a href="mailto:jean-marie@dautelle.com">Jean-Marie Dautelle</a>
 * @version 1.0, April 15, 2009
 */
public abstract class Measure<Q extends Quantity> implements Measurable<Q>,
		Serializable {

	/**
	 * Default constructor.
	 */
	protected Measure() {
	}

	/**
	 * Returns the measurement numeric value.
	 * 
	 * @return the measurement value.
	 */
	public abstract Number getValue();

	/**
	 * Returns the measurement unit.
	 * 
	 * @return the measurement unit.
	 */
	public abstract Unit<Q> getUnit();

	/**
	 * Convenient method equivalent to {@link #to(javax.measure.unit.Unit)
	 * to(this.getUnit().toSI())}.
	 * 
	 * @return this measure or a new measure equivalent to this measure but
	 *         stated in SI units.
	 * @throws ArithmeticException if the result is inexact and the quotient 
	 *         has a non-terminating decimal expansion.
	 */
	public Measure<Q> toSI() {
		return to(this.getUnit().toSI());
	}

	/**
	 * Returns this measure after conversion to specified unit. The default
	 * implementation returns
	 * <code>Measure.valueOf(doubleValue(unit), unit)</code>. If this measure is
	 * already stated in the specified unit, then this measure is returned and
	 * no conversion is performed.
	 * 
	 * @param unit the unit in which the returned measure is stated.
	 * @return this measure or a new measure equivalent to this measure but
	 *         stated in the specified unit.
	 * @throws ArithmeticException if the result is inexact and the quotient has
	 *         a
	 *             non-terminating decimal expansion.
	 */
	public Measure<Q> to(Unit<Q> unit) {
		if (unit.equals(this.getUnit()))
			return this;
		return Measure.valueOf(doubleValue(unit), unit);
	}

	/**
	 * Returns this measure after conversion to specified unit. The default
	 * implementation returns
	 * <code>Measure.valueOf(decimalValue(unit, ctx), unit)</code>. If this
	 * measure is already stated in the specified unit, then this measure is
	 * returned and no conversion is performed.
	 * 
	 * @param unit
	 *            the unit in which the returned measure is stated.
	 * @param ctx
	 *            the math context to use for conversion.
	 * @return this measure or a new measure equivalent to this measure but
	 *         stated in the specified unit.
	 * @throws ArithmeticException
	 *             if the result is inexact but the rounding mode is
	 *             <code>UNNECESSARY</code> or
	 *             <code>mathContext.precision == 0</code> and the quotient has
	 *             a non-terminating decimal expansion.
	 */
	public Measure<Q> to(Unit<Q> unit, MathContext ctx) {
		if (unit.equals(this.getUnit()))
			return this;
		return Measure.valueOf(decimalValue(unit, ctx), unit);
	}

	/**
	 * Compares this measure and the specified measurable to the given accuracy.
	 * Measurements are considered approximately equals if their absolute
	 * differences when stated in the same unit is less than the specified
	 * accuracy.
	 * 
	 * @param that
	 *            the measurable to compare with.
	 * @param accuracy
	 *            the absolute error allowed.
	 * @return
	 *         <code>abs(this.doubleValue(getUnit())- that.doubleValue(getUnit())) &lt;= accuracy</code>
	 */
	public boolean approximates(Measurable<Q> that, double accuracy) {
		Unit<Q> unit = this.getUnit();
		return Math.abs(this.doubleValue(unit) - that.doubleValue(unit)) <= accuracy;
	}

	/**
	 * Compares this measure to the specified measurable quantity. The default
	 * implementation compares the {@link Measurable#doubleValue(Unit)} of both
	 * this measure and the specified measurable stated in the same unit (this
	 * measure's {@link #getUnit() unit}).
	 * 
	 * @return a negative integer, zero, or a positive integer as this measure
	 *         is less than, equal to, or greater than the specified measurable
	 *         quantity.
	 * @return <code>Double.compare(this.doubleValue(getUnit()),
	 *         that.doubleValue(getUnit()))</code>
	 */
	public int compareTo(Measurable<Q> that) {
		Unit<Q> unit = getUnit();
		return Double.compare(doubleValue(unit), that.doubleValue(unit));
	}

	/**
	 * Compares this measure against the specified object for <b>strict</b>
	 * equality (same unit and same amount).
	 * 
	 * <p> Similarly to the {@link BigDecimal#equals} method which consider 2.0
	 *     and 2.00 as different objects because of different internal scales, 
	 *     measurements such as <code>Measure.valueOf(3.0, KILOGRAM)</code>
	 *     <code>Measure.valueOf(3, KILOGRAM)</code> and
	 *     <code>Measure.valueOf("3 kg")</code> might not be considered equals
	 *     because of possible differences in their implementations.</p>
	 * 
	 * <p> To compare measures stated using different units or using different
	 *     amount implementations the {@link #compareTo} or 
	 *     {@link #approximates} methods should be used.</p>
	 * 
	 * @param obj the object to compare with.
	 * @return <code>this.getUnit.equals(obj.getUnit()) 
	 *         && this.getValue().equals(obj.getValue())</code>
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Measure))
			return false;
		Measure<?> that = (Measure<?>) obj;
		return this.getUnit().equals(that.getUnit())
				&& this.getValue().equals(that.getValue());
	}

	/**
	 * Returns the hash code for this measure.
	 * 
	 * @return the hash code value.
	 */
	@Override
	public int hashCode() {
		return getUnit().hashCode() + getValue().hashCode();
	}

	/**
	 * Returns the <code>String</code> representation of this measure. The
	 * string produced for a given measure is always the same; it is not
	 * affected by locale. This means that it can be used as a canonical string
	 * representation for exchanging measure, or as a key for a Hashtable, etc.
	 * Locale-sensitive measure formatting and parsing is handled by the
	 * {@link MeasureFormat} class and its subclasses.
	 * 
	 * @return <code>UnitFormat.getInternational().format(this)</code>
	 */
	@Override
	public final String toString() {
		return MeasureFormat.getStandard().format(this);
	}

	// Implements Measurable
	public final int intValue(Unit<Q> unit) throws ArithmeticException {
		long longValue = longValue(unit);
		if ((longValue < Integer.MIN_VALUE) || (longValue > Integer.MAX_VALUE))
			throw new ArithmeticException("Cannot convert " + longValue
					+ " to int (overflow)");
		return (int) longValue;
	}

	// Implements Measurable
	public long longValue(Unit<Q> unit) throws ArithmeticException {
		double result = doubleValue(unit);
		if ((result < Long.MIN_VALUE) || (result > Long.MAX_VALUE))
			throw new ArithmeticException("Overflow (" + result + ")");
		return (long) result;
	}

	// Implements Measurable
	public final float floatValue(Unit<Q> unit) {
		return (float) doubleValue(unit);
	}

	/**
	 * Casts this measure to a parameterized unit of specified nature or throw a
	 * <code>ClassCastException</code> if the dimension of the specified
	 * quantity and this measure unit's dimension do not match. For
	 * example:[code] 
	 *     Measure<Length> length = Measure.valueOf("2 km").asType(Length.class);
	 * [/code]
	 * 
	 * @param type the quantity class identifying the nature of the measure.
	 * @return this measure parameterized with the specified type.
	 * @throws ClassCastException if the dimension of this unit is different 
	 *         from the specified quantity dimension.
	 * @throws UnsupportedOperationException
	 *             if the specified quantity class does not have a public static
	 *             field named "UNIT" holding the SI unit for the quantity.
	 * @see Unit#asType(Class)
	 */
	@SuppressWarnings("unchecked")
	public final <T extends Quantity> Measure<T> asType(Class<T> type)
			throws ClassCastException {
		this.getUnit().asType(type); // Raises ClassCastException is dimension
										// mismatches.
		return (Measure<T>) this;
	}

	/**
	 * Returns the
	 * {@link #valueOf(java.math.BigDecimal, javax.measure.unit.Unit) decimal}
	 * measure of unknown type corresponding to the specified representation.
	 * This method can be used to parse dimensionless quantities.[code]
	 *     Measure<Dimensionless> proportion = Measure.valueOf("0.234").asType(Dimensionless.class);
	 * [/code]
	 * 
	 * <p> Note: This method handles only
	 * {@link javax.measure.unit.UnitFormat#getStandard standard} unit format
	 * (<a href="http://unitsofmeasure.org/">UCUM</a> based). Locale-sensitive
	 * measure formatting and parsing are handled by the {@link MeasureFormat}
	 * class and its subclasses.</p>
	 * 
	 * @param csq the decimal value and its unit (if any) separated by space(s).
	 * @return <code>MeasureFormat.getInternational().parse(csq, new ParsePosition(0))</code>
	 */
	public static Measure<?> valueOf(CharSequence csq) {
		return MeasureFormat.getStandard().parse(csq, new ParsePosition(0));
	}

	/**
	 * Returns the scalar measure for the specified <code>int</code> stated in
	 * the specified unit.
	 * 
	 * @param intValue the measurement value.
	 * @param unit the measurement unit.
	 * @return the corresponding <code>int</code> measure.
	 */
	public static <Q extends Quantity> Measure<Q> valueOf(int intValue,
			Unit<Q> unit) {
		return new IntegerMeasure<Q>(intValue, unit);
	}

	private static class IntegerMeasure<T extends Quantity> extends Measure<T> {

		int _value;

		Unit<T> _unit;

		public IntegerMeasure(int value, Unit<T> unit) {
			_value = value;
			_unit = unit;
		}

		@Override
		public Integer getValue() {
			return _value;
		}

		@Override
		public Unit<T> getUnit() {
			return _unit;
		}

		// Implements Measurable
		public double doubleValue(Unit<T> unit) {
			return (_unit.equals(unit)) ? _value : _unit.getConverterTo(unit)
					.convert(_value);
		}

		// Implements Measurable
		public BigDecimal decimalValue(Unit<T> unit, MathContext ctx)
				throws ArithmeticException {
			BigDecimal decimal = BigDecimal.valueOf(_value);
			return (_unit.equals(unit)) ? decimal : _unit.getConverterTo(unit)
					.convert(decimal, ctx);
		}

		private static final long serialVersionUID = 1L;

	}

	/**
	 * Returns the scalar measure for the specified <code>long</code> stated in
	 * the specified unit.
	 * 
	 * @param longValue the measurement value.
	 * @param unit the measurement unit.
	 * @return the corresponding <code>long</code> measure.
	 */
	public static <Q extends Quantity> Measure<Q> valueOf(long longValue,
			Unit<Q> unit) {
		return new LongMeasure<Q>(longValue, unit);
	}

	private static class LongMeasure<T extends Quantity> extends Measure<T> {

		long _value;

		Unit<T> _unit;

		public LongMeasure(long value, Unit<T> unit) {
			_value = value;
			_unit = unit;
		}

		@Override
		public Long getValue() {
			return _value;
		}

		@Override
		public Unit<T> getUnit() {
			return _unit;
		}

		@Override
		// Avoid loss of precision if no conversion.
		public long longValue(Unit<T> unit) {
			return ((_unit.equals(unit)) || (_unit.getConverterTo(unit) == UnitConverter.IDENTITY)) ? _value
					: super.longValue(unit);
		}

		// Implements Measurable
		public double doubleValue(Unit<T> unit) {
			return (_unit.equals(unit)) ? _value : _unit.getConverterTo(unit)
					.convert(_value);
		}

		// Implements Measurable
		public BigDecimal decimalValue(Unit<T> unit, MathContext ctx)
				throws ArithmeticException {
			BigDecimal decimal = BigDecimal.valueOf(_value);
			return (_unit.equals(unit)) ? decimal : _unit.getConverterTo(unit)
					.convert(decimal, ctx);
		}

		private static final long serialVersionUID = 1L;

	}

	/**
	 * Returns the scalar measure for the specified <code>float</code> stated in
	 * the specified unit.
	 * 
	 * @param floatValue the measurement value.
	 * @param unit the measurement unit.
	 * @return the corresponding <code>float</code> measure.
	 */
	public static <Q extends Quantity> Measure<Q> valueOf(float floatValue,
			Unit<Q> unit) {
		return new FloatMeasure<Q>(floatValue, unit);
	}

	private static class FloatMeasure<T extends Quantity> extends Measure<T> {

		float _value;

		Unit<T> _unit;

		public FloatMeasure(float value, Unit<T> unit) {
			_value = value;
			_unit = unit;
		}

		@Override
		public Float getValue() {
			return _value;
		}

		@Override
		public Unit<T> getUnit() {
			return _unit;
		}

		// Implements Measurable
		public double doubleValue(Unit<T> unit) {
			return (_unit.equals(unit)) ? _value : _unit.getConverterTo(unit)
					.convert(_value);
		}

		// Implements Measurable
		public BigDecimal decimalValue(Unit<T> unit, MathContext ctx)
				throws ArithmeticException {
			BigDecimal decimal = BigDecimal.valueOf(_value);
			return (_unit.equals(unit)) ? decimal : _unit.getConverterTo(unit)
					.convert(decimal, ctx);
		}

		private static final long serialVersionUID = 1L;

	}

	/**
	 * Returns the scalar measure for the specified <code>double</code> stated
	 * in the specified unit.
	 * 
	 * @param doubleValue the measurement value.
	 * @param unit the measurement unit.
	 * @return the corresponding <code>double</code> measure.
	 */
	public static <Q extends Quantity> Measure<Q> valueOf(double doubleValue,
			Unit<Q> unit) {
		return new DoubleMeasure<Q>(doubleValue, unit);
	}

	private static class DoubleMeasure<T extends Quantity> extends Measure<T> {

		double _value;

		Unit<T> _unit;

		public DoubleMeasure(double value, Unit<T> unit) {
			_value = value;
			_unit = unit;
		}

		@Override
		public Double getValue() {
			return _value;
		}

		@Override
		public Unit<T> getUnit() {
			return _unit;
		}

		// Implements Measurable
		public double doubleValue(Unit<T> unit) {
			return (_unit.equals(unit)) ? _value : _unit.getConverterTo(unit)
					.convert(_value);
		}

		// Implements Measurable
		public BigDecimal decimalValue(Unit<T> unit, MathContext ctx)
				throws ArithmeticException {
			BigDecimal decimal = BigDecimal.valueOf(_value);
			return (_unit.equals(unit)) ? decimal : _unit.getConverterTo(unit)
					.convert(decimal, ctx);
		}

		private static final long serialVersionUID = 1L;

	}

	/**
	 * Returns the scalar measure for the specified <code>BigDecimal</code>
	 * stated in the specified unit.
	 * 
	 * @param decimalValue the measurement value.
	 * @param unit the measurement unit.
	 * @return the corresponding <code>BigDecimal</code> measure.
	 */
	public static <Q extends Quantity> Measure<Q> valueOf(
			BigDecimal decimalValue, Unit<Q> unit) {
		return new DecimalMeasure<Q>(decimalValue, unit);
	}

	private static class DecimalMeasure<T extends Quantity> extends Measure<T> {

		BigDecimal _value;

		Unit<T> _unit;

		public DecimalMeasure(BigDecimal value, Unit<T> unit) {
			_value = value;
			_unit = unit;
		}

		@Override
		public BigDecimal getValue() {
			return _value;
		}

		@Override
		public Unit<T> getUnit() {
			return _unit;
		}

		// Implements Measurable
		public double doubleValue(Unit<T> unit) {
			return (_unit.equals(unit)) ? _value.doubleValue() : _unit
					.getConverterTo(unit).convert(_value.doubleValue());
		}

		// Implements Measurable
		public BigDecimal decimalValue(Unit<T> unit, MathContext ctx)
				throws ArithmeticException {
			return (_unit.equals(unit)) ? _value : _unit.getConverterTo(unit)
					.convert(_value, ctx);
		}

		private static final long serialVersionUID = 1L;

	}

	private static final long serialVersionUID = 1L;

}