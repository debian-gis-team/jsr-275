/*
 * JScience - Java Tools and Libraries for the Advancement of Sciences
 * Copyright (c) 2005-2009, JScience (http://jscience.org/)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package javax.measure;

import java.math.BigDecimal;
import java.math.MathContext;
import javax.measure.quantity.Quantity;
import javax.measure.unit.Unit;

/**
 * <p> This interface represents the measurable, countable, or comparable 
 *     property or aspect of a thing.</p>
 *     
 * <p> Measurable instances are for the most part scalar quantities.[code]
 *     class Delay implements Measurable<Duration> {
 *          private final double seconds; // Implicit internal unit.
 *          public Delay(double value, Unit<Duration> unit) { 
 *              seconds = unit.getConverterTo(SI.SECOND).convert(value);
 *          }
 *          public double doubleValue(Unit<Duration> unit) { 
 *              return SI.SECOND.getConverterTo(unit).convert(seconds);
 *          }
 *          ...
 *     }
 *     Thread.wait(new Delay(24, NonSI.HOUR)); // Assuming Thread.wait(Measurable<Duration>) method.
 *     [/code]
 *     Non-scalar quantities are nevertheless allowed as long as an aggregate
 *     value makes sense.[code]
 *     class Velocity3D implements Measurable<Velocity> {
 *          private double x, y, z; // Meters per second.
 *          public double doubleValue(Unit<Velocity> unit) { // Returns the vector norm.
 *              double meterPerSecond = Math.sqrt(x * x + y * y + z * z);
 *              return SI.METER_PER_SECOND.getConverterTo(unit).convert(meterPerSecond);
 *          }
 *          ...
 *     }
 *     class ComplexCurrent implements extends Measurable<ElectricCurrent> {
 *          private Complex amperes;
 *          public double doubleValue(Unit<ElectricCurrent> unit) { // Returns the magnitude.
 *              return AMPERE.getConverterTo(unit).convert(amperes.magnitude());
 *          }
 *          ...
 *          public Complex complexValue(Unit<ElectricCurrent> unit) { ... }
 *     } [/code]</p>
 *
 *   <p> For convenience, measurable instances of any type can be created
 *       using the {@link Measure} factory methods.[code]
 *       Thread.wait(Measure.valueOf(24, NonSI.HOUR));[/code]</p>
 *
 * @author  <a href="mailto:jean-marie@dautelle.com">Jean-Marie Dautelle</a>
 * @version 1.0, April 15, 2009
 */
public interface Measurable<Q extends Quantity> extends Comparable<Measurable<Q>> {

    /**
     * Returns the integral <code>int</code> value of this measurable when
     * stated in the specified unit.
     *
     * <p> Note: This method differs from the <code>Number.intValue()</code>
     *           in the sense that an ArithmeticException is raised instead
     *           of a bit truncation in case of overflow (safety critical).</p>
     *
     * @param unit the unit in which the returned value is stated.
     * @return the numeric value after conversion to type <code>int</code>.
     * @throws ArithmeticException if this measurable cannot be represented
     *         by a <code>int</code> number in the specified unit.
     */
    int intValue(Unit<Q> unit) throws ArithmeticException;

    /**
     * Returns the integral <code>long</code> value of this measurable when
     * stated in the specified unit.
     *
     * <p> Note: This method differs from the <code>Number.longValue()</code>
     *           in the sense that an ArithmeticException is raised instead
     *           of a bit truncation in case of overflow (safety critical).</p>
     *
     * @param unit the unit in which the returned value is stated.
     * @return the numeric value after conversion to type <code>long</code>.
     * @throws ArithmeticException if this measurable cannot be represented
     *         by a <code>int</code> number in the specified unit.
     */
    long longValue(Unit<Q> unit) throws ArithmeticException;

    /**
     * Returns the <code>float</code> value of this measurable
     * when stated in the specified unit. If the measurable has too great of
     * a magnitude to be represented as a <code>float</code>,
     * <code>FLOAT.NEGATIVE_INFINITY</code> or
     * <code>FLOAT.POSITIVE_INFINITY</code> is returned as appropriate.
     *
     * @param unit the unit in which this returned value is stated.
     * @return the numeric value after conversion to type <code>float</code>.
     */
    float floatValue(Unit<Q> unit);

    /**
     * Returns the <code>double</code> value of this measurable
     * when stated in the specified unit. If the measurable has too great of
     * a magnitude to be represented as a <code>double</code>,
     * <code>Double.NEGATIVE_INFINITY</code> or
     * <code>Double.POSITIVE_INFINITY</code> is returned as appropriate.
     * 
     * @param unit the unit in which this returned value is stated.
     * @return the numeric value after conversion to type <code>double</code>.
     */
    double doubleValue(Unit<Q> unit);

    /**
     * Returns the <code>BigDecimal</code> value of this measurable when
     * stated in the specified unit.
     * 
     * @param unit the unit in which the returned value is stated.
     * @param ctx the math context being used for conversion.
     * @return the decimal value after conversion.
     * @throws ArithmeticException if the result is inexact but the
     *         rounding mode is <code>UNNECESSARY</code> or 
     *         <code>mathContext.precision == 0</code> and the quotient has a 
     *         non-terminating decimal expansion.
     */
    BigDecimal decimalValue(Unit<Q> unit, MathContext ctx) throws ArithmeticException;

}