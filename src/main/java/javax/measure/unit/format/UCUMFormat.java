/*
 * JScience - Java Tools and Libraries for the Advancement of Sciences
 * Copyright (c) 2005-2009, JScience (http://jscience.org/)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package javax.measure.unit.format;

import javax.measure.unit.UnitFormat;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.text.*;
import java.util.ResourceBundle;
import javax.measure.converter.MultiplyConverter;
import javax.measure.converter.RationalConverter;
import javax.measure.converter.UnitConverter;
import javax.measure.unit.*;
import javax.measure.quantity.Quantity;

/**
 * <p>
 * This class provides the interface for formatting and parsing
 * {@link javax.measure.unit.Unit units} according to the <a
 * href="http://unitsofmeasure.org/">Uniform Code for Units of Measure</a>
 * (UCUM).
 * </p>
 * 
 * <p>
 * For a technical/historical overview of this format please read <a
 * href="http://www.pubmedcentral.nih.gov/articlerender.fcgi?artid=61354"> Units
 * of Measure in Clinical Information Systems</a>.
 * </p>
 * 
 * <p>
 * As of revision 1.16, the BNF in the UCUM standard contains an <a
 * href="http://unitsofmeasure.org/ticket/4">error</a>. I've attempted to work
 * around the problem by modifying the BNF productions for &lt;Term&gt;. Once
 * the error in the standard is corrected, it may be necessary to modify the
 * productions in the UCUMParser.jj file to conform to the standard.
 * </p>
 * 
 * @author <a href="mailto:eric-r@northwestern.edu">Eric Russell</a>
 * @version 1.0
 */
public abstract class UCUMFormat extends UnitFormat {

	// /////////////////
	// Class methods //
	// /////////////////
	/** Returns the instance for formatting using "print" symbols */
	public static UCUMFormat getPrintInstance() {
		return Print.DEFAULT;
	}

	/** Returns the instance for formatting using user defined symbols */
	public static UCUMFormat getPrintInstance(SymbolMap symbolMap) {
		return new Print(symbolMap);
	}

	/**
	 * Returns the instance for formatting and parsing using case sensitive
	 * symbols
	 */
	public static UCUMFormat getCaseSensitiveInstance() {
		return Parsing.DEFAULT_CS;
	}

	/**
	 * Returns a case sensitive instance for formatting and parsing using user
	 * defined symbols
	 */
	public static UCUMFormat getCaseSensitiveInstance(SymbolMap symbolMap) {
		return new Parsing(symbolMap, true);
	}

	/**
	 * Returns the instance for formatting and parsing using case insensitive
	 * symbols
	 */
	public static UCUMFormat getCaseInsensitiveInstance() {
		return Parsing.DEFAULT_CI;
	}

	/**
	 * Returns a case insensitive instance for formatting and parsing using user
	 * defined symbols
	 */
	public static UCUMFormat getCaseInsensitiveInstance(SymbolMap symbolMap) {
		return new Parsing(symbolMap, false);
	}

	/**
	 * The symbol map used by this instance to map between
	 * {@link javax.measure.unit.Unit Unit}s and <code>String</code>s.
	 */
	final SymbolMap _symbolMap;

	// ////////////////
	// Constructors //
	// ////////////////
	/**
	 * Base constructor.
	 */
	UCUMFormat(SymbolMap symbolMap) {
		_symbolMap = symbolMap;
	}

	// //////////////
	// Formatting //
	// //////////////
	public Appendable format(Unit<?> unit, Appendable appendable)
			throws IOException {
		CharSequence symbol;
		CharSequence annotation = null;
		if (unit instanceof AnnotatedUnit) {
			unit = ((AnnotatedUnit<?>) unit).getRealUnit();
			annotation = ((AnnotatedUnit<?>) unit).getAnnotation();
		}
		String mapSymbol = _symbolMap.getSymbol(unit);
		if (mapSymbol != null) {
			symbol = mapSymbol;
		} else if (unit instanceof ProductUnit) {
			ProductUnit<?> productUnit = (ProductUnit<?>) unit;
			StringBuffer app = new StringBuffer();
			for (int i = 0; i < productUnit.getUnitCount(); i++) {
				if (productUnit.getUnitRoot(i) != 1) {
					throw new IllegalArgumentException(
							"Unable to format units in UCUM (fractional powers not supported)");
				}
				StringBuffer temp = new StringBuffer();
				temp = (StringBuffer) format(productUnit.getUnit(i), temp);
				if ((temp.indexOf(".") >= 0) || (temp.indexOf("/") >= 0)) {
					temp.insert(0, '(');
					temp.append(')');
				}
				int pow = productUnit.getUnitPow(i);
				if (i > 0) {
					if (pow >= 0) {
						app.append('.');
					} else if (i < (productUnit.getUnitCount() - 1)) {
						app.append('.');
					} else {
						app.append('/');
						pow = -pow;
					}
				} else if (pow < 0) {
					app.append('/');
					pow = -pow;
				}
				app.append(temp);
				if (pow != 1) {
					app.append(Integer.toString(pow));
				}
			}
			symbol = app;
		} else if ((unit instanceof TransformedUnit)
				|| unit.equals(SI.KILOGRAM)) {
			StringBuffer temp = new StringBuffer();
			UnitConverter converter;
			boolean printSeparator;
			if (unit.equals(SI.KILOGRAM)) {
				// A special case because KILOGRAM is a BaseUnit instead of
				// a transformed unit, for compatability with existing SI
				// unit system.
				temp = format(UCUM.GRAM, temp, new FieldPosition(0));
				converter = Prefix.KILO.getConverter();
				printSeparator = true;
			} else {
				TransformedUnit<?> transformedUnit = (TransformedUnit<?>) unit;
				Unit<?> parentUnits = transformedUnit.getParentUnit();
				converter = transformedUnit.toParentUnit();
				if (parentUnits.equals(SI.KILOGRAM)) {
					// More special-case hackery to work around gram/kilogram
					// incosistency
					parentUnits = UCUM.GRAM;
					converter = converter.concatenate(Prefix.KILO
							.getConverter());
				}
				temp = format(parentUnits, temp, new FieldPosition(0));
				printSeparator = !parentUnits.equals(Unit.ONE);
			}
			formatConverter(converter, printSeparator, temp);
			symbol = temp;
		} else if (unit instanceof BaseUnit) {
			symbol = ((BaseUnit<?>) unit).getSymbol();
		} else if (unit instanceof AlternateUnit) {
			symbol = ((AlternateUnit<?>) unit).getSymbol();
		} else {
			throw new IllegalArgumentException(
					"Cannot format the given Object as UCUM units (unsupported unit "
							+ unit.getClass().getName()
							+ "). "
							+ "Custom units types should override the toString() method as the default implementation uses the UCUM format.");
		}

		appendable.append(symbol);
		if (annotation != null && annotation.length() > 0) {
			appendAnnotation(unit, symbol, annotation, appendable);
		}

		return appendable;
	}

	void appendAnnotation(Unit<?> unit, CharSequence symbol,
			CharSequence annotation, Appendable appendable) throws IOException {
		appendable.append('{');
		appendable.append(annotation);
		appendable.append('}');
	}

	/**
	 * Formats the given converter to the given StringBuffer. This is similar to
	 * what {@link ConverterFormat} does, but there's no need to worry about
	 * operator precedence here, since UCUM only supports multiplication,
	 * division, and exponentiation and expressions are always evaluated left-
	 * to-right.
	 * 
	 * @param converter
	 *            the converter to be formatted
	 * @param continued
	 *            <code>true</code> if the converter expression should begin
	 *            with an operator, otherwise <code>false</code>. This will
	 *            always be true unless the unit being modified is equal to
	 *            Unit.ONE.
	 * @param buffer
	 *            the <code>StringBuffer</code> to append to. Contains the
	 *            already-formatted unit being modified by the given converter.
	 */
	void formatConverter(UnitConverter converter, boolean continued,
			StringBuffer buffer) {
		boolean unitIsExpression = ((buffer.indexOf(".") >= 0) || (buffer
				.indexOf("/") >= 0));
		Prefix prefix = _symbolMap.getPrefix(converter);
		if ((prefix != null) && (!unitIsExpression)) {
			buffer.insert(0, _symbolMap.getSymbol(prefix));
		} else if (converter == UnitConverter.IDENTITY) {
			// do nothing
		} else if (converter instanceof MultiplyConverter) {
			if (unitIsExpression) {
				buffer.insert(0, '(');
				buffer.append(')');
			}
			MultiplyConverter multiplyConverter = (MultiplyConverter) converter;
			double factor = multiplyConverter.getFactor();
			long lFactor = (long) factor;
			if ((lFactor != factor) || (lFactor < -9007199254740992L)
					|| (lFactor > 9007199254740992L)) {
				throw new IllegalArgumentException(
						"Only integer factors are supported in UCUM");
			}
			if (continued) {
				buffer.append('.');
			}
			buffer.append(lFactor);
		} else if (converter instanceof RationalConverter) {
			if (unitIsExpression) {
				buffer.insert(0, '(');
				buffer.append(')');
			}
			RationalConverter rationalConverter = (RationalConverter) converter;
			if (!rationalConverter.getDividend().equals(BigInteger.ONE)) {
				if (continued) {
					buffer.append('.');
				}
				buffer.append(rationalConverter.getDividend());
			}
			if (!rationalConverter.getDivisor().equals(BigInteger.ONE)) {
				buffer.append('/');
				buffer.append(rationalConverter.getDivisor());
			}
		} else {
			throw new IllegalArgumentException(
					"Unable to format units in UCUM (unsupported UnitConverter "
							+ converter + ")");
		}
	}

	// /////////////////
	// Inner classes //
	// /////////////////
	/**
	 * The Print format is used to output units according to the "print" column
	 * in the UCUM standard. Because "print" symbols in UCUM are not unique,
	 * this class of UCUMFormat may not be used for parsing, only for
	 * formatting.
	 */
	private static class Print extends UCUMFormat {

		private static final SymbolMap PRINT_SYMBOLS = new SymbolMap(
				ResourceBundle
						.getBundle("javax.measure.unit.format.UCUM_Print"));
		private static final Print DEFAULT = new Print(PRINT_SYMBOLS);

		public Print(SymbolMap symbols) {
			super(symbols);
		}

		@Override
		public Unit<? extends Quantity> parse(CharSequence csq,
				ParsePosition pos) throws IllegalArgumentException {
			throw new UnsupportedOperationException(
					"The print format is for pretty-printing of units only. Parsing is not supported.");
		}

		@Override
		void appendAnnotation(Unit<?> unit, CharSequence symbol,
				CharSequence annotation, Appendable appendable)
				throws IOException {
			if (symbol != null && symbol.length() > 0) {
				appendable.append('(');
				appendable.append(annotation);
				appendable.append(')');
			} else {
				appendable.append(annotation);
			}
		}

		private static final long serialVersionUID = 1L;

	}

	/**
	 * The Parsing format outputs formats and parses units according to the
	 * "c/s" or "c/i" column in the UCUM standard, depending on which SymbolMap
	 * is passed to its constructor.
	 */
	private static class Parsing extends UCUMFormat {

		private static final SymbolMap CASE_SENSITIVE_SYMBOLS = new SymbolMap(
				ResourceBundle.getBundle("javax.measure.unit.format.UCUM_CS"));
		private static final SymbolMap CASE_INSENSITIVE_SYMBOLS = new SymbolMap(
				ResourceBundle.getBundle("javax.measure.unit.format.UCUM_CI"));
		private static final Parsing DEFAULT_CS = new Parsing(
				CASE_SENSITIVE_SYMBOLS, true);
		private static final Parsing DEFAULT_CI = new Parsing(
				CASE_INSENSITIVE_SYMBOLS, false);
		private final boolean _caseSensitive;

		public Parsing(SymbolMap symbols, boolean caseSensitive) {
			super(symbols);
			_caseSensitive = caseSensitive;
		}

		@Override
		public Unit<? extends Quantity> parse(CharSequence csq,
				ParsePosition cursor) throws IllegalArgumentException {
			// Parsing reads the whole character sequence from the parse
			// position.
			int start = cursor.getIndex();
			int end = csq.length();
			if (end <= start)
				return Unit.ONE;
			String source = csq.subSequence(start, end).toString().trim();
			if (source.length() == 0)
				return Unit.ONE;
			if (!_caseSensitive) {
				source = source.toUpperCase();
			}
			UCUMParser parser = new UCUMParser(_symbolMap,
					new ByteArrayInputStream(source.getBytes()));
			try {
				Unit<?> result = parser.parseUnit();
				cursor.setIndex(end);
				return result;
			} catch (javax.measure.unit.format.ParseException e) {
				if (e.currentToken != null) {
					cursor.setErrorIndex(start + e.currentToken.endColumn);
				} else {
					cursor.setErrorIndex(start);
				}
				throw new IllegalArgumentException(e.getMessage());
			} catch (TokenMgrError e) {
				cursor.setErrorIndex(start);
				throw new IllegalArgumentException(e.getMessage());
			}
		}

		private static final long serialVersionUID = 1L;

	}

	private static final long serialVersionUID = 1L;
}
