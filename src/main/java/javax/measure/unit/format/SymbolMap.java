/*
 * JScience - Java Tools and Libraries for the Advancement of Sciences
 * Copyright (c) 2005-2009, JScience (http://jscience.org/)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package javax.measure.unit.format;

import java.lang.reflect.Field;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import javax.measure.converter.UnitConverter;
import javax.measure.unit.Unit;

/**
 * <p> The SymbolMap class provides a set of mappings between
 *     {@link javax.measure.unit.Unit Units} and symbols (both ways),
 *     between {@link Prefix Prefix}es and symbols
 *     (both ways), and from {@link javax.measure.converter.UnitConverter
 *     UnitConverter}s to {@link Prefix Prefix}es (one way).
 *     No attempt is made to verify the uniqueness of the mappings.</p>
 *
 * <p> Mappings are read from a <code>ResourceBundle</code>, the keys
 *     of which should consist of a fully-qualified class name, followed
 *     by a dot ('.'), and then the name of a static field belonging
 *     to that class, followed optionally by another dot and a number.
 *     If the trailing dot and number are not present, the value
 *     associated with the key is treated as a
 *     {@link SymbolMap#label(javax.measure.unit.Unit, String) label},
 *     otherwise if the trailing dot and number are present, the value
 *     is treated as an {@link SymbolMap#alias(javax.measure.unit.Unit,String) alias}.
 *     Aliases map from String to Unit only, whereas labels map in both
 *     directions. A given unit may have any number of aliases, but may
 *     have only one label.</p>
 * 
 * @author <a href="mailto:eric-r@northwestern.edu">Eric Russell</a>
 * @version 1.0
 */
public class SymbolMap {
    
    private Map<String, Unit<?>> _symbolToUnit;
    private Map<Unit<?>, String> _unitToSymbol;
    private Map<String, Object> _symbolToPrefix;
    private Map<Object, String> _prefixToSymbol;
    private Map<UnitConverter, Prefix> _converterToPrefix;
    
    /**
     * Creates an empty mapping.
     */
    public SymbolMap () {
        _symbolToUnit = new HashMap<String, Unit<?>>();
        _unitToSymbol = new HashMap<Unit<?>, String>();
        _symbolToPrefix = new HashMap<String, Object>();
        _prefixToSymbol = new HashMap<Object, String>();
        _converterToPrefix = new HashMap<UnitConverter, Prefix>();
    }

    /** 
     * Creates a symbol map from the specified resource bundle,
     *
     * @param rb the resource bundle.
     */
    public SymbolMap (ResourceBundle rb) {
        this();
        for (Enumeration<String> i = rb.getKeys(); i.hasMoreElements();) {
            String fqn = i.nextElement();
            String symbol = rb.getString(fqn);
            boolean isAlias = false;
            int lastDot = fqn.lastIndexOf('.');
            String className = fqn.substring(0, lastDot);
            String fieldName = fqn.substring(lastDot+1, fqn.length());
            if (Character.isDigit(fieldName.charAt(0))) {
                isAlias = true;
                fqn = className;
                lastDot = fqn.lastIndexOf('.');
                className = fqn.substring(0, lastDot);
                fieldName = fqn.substring(lastDot+1, fqn.length());
            }
            try {
                Class<?> c = Class.forName(className);
                Field field = c.getField(fieldName);
                Object value = field.get(null);
                if (value instanceof Unit) {
                    if (isAlias) {
                        alias((Unit<?>)value, symbol);
                    } else {
                        label((Unit<?>)value, symbol);
                    }
                } else if (value instanceof Prefix) {
                    label((Prefix)value, symbol);
                } else {
                    throw new ClassCastException("unable to cast "+value+" to Unit or Prefix");
                }
            } catch (Exception e) {
                System.err.println("ERROR reading Unit names: " + e.toString());
            }
        }
    }

    /**
     * Attaches a label to the specified unit. For example:[code]
     *    symbolMap.label(DAY.multiply(365), "year");
     *    symbolMap.label(NonSI.FOOT, "ft");
     * [/code]
     *
     * @param unit the unit to label.
     * @param symbol the new symbol for the unit.
     */
    public void label (Unit<?> unit, String symbol) {
        _symbolToUnit.put(symbol, unit);
        _unitToSymbol.put(unit, symbol);
    }
    
    /**
     * Attaches an alias to the specified unit. Multiple aliases may be
     * attached to the same unit. Aliases are used during parsing to
     * recognize different variants of the same unit.[code]
     *     symbolMap.alias(NonSI.FOOT, "foot");
     *     symbolMap.alias(NonSI.FOOT, "feet");
     *     symbolMap.alias(SI.METER, "meter");
     *     symbolMap.alias(SI.METER, "metre");
     * [/code]
     *
     * @param unit the unit to label.
     * @param symbol the new symbol for the unit.
     */
    public void alias (Unit<?> unit, String symbol) { 
        _symbolToUnit.put(symbol, unit);
    }
    
    /**
     * Attaches a label to the specified prefix. For example:[code]
     *    symbolMap.label(Prefix.GIGA, "G");
     *    symbolMap.label(Prefix.MICRO, "µ");
     * [/code]
     */
    public void label(Prefix prefix, String symbol) {
        _symbolToPrefix.put(symbol, prefix);
        _prefixToSymbol.put(prefix, symbol);
        _converterToPrefix.put(prefix.getConverter(), prefix);
    }
    
    /**
     * Returns the unit for the specified symbol.
     * 
     * @param symbol the symbol.
     * @return the corresponding unit or <code>null</code> if none.
     */
    public Unit<?> getUnit (String symbol) {
        return _symbolToUnit.get(symbol);
    }
    
    /**
     * Returns the symbol (label) for the specified unit.
     *
     * @param unit the corresponding symbol.
     * @return the corresponding symbol or <code>null</code> if none.
     */
    public String getSymbol (Unit<?> unit) {
        return _unitToSymbol.get(unit);
    }
    
    /**
     * Returns the prefix (if any) for the specified symbol.
     *
     * @param symbol the unit symbol.
     * @return the corresponding prefix or <code>null</code> if none.
     */
    public Prefix getPrefix (String symbol) {
        for (Iterator<String> i = _symbolToPrefix.keySet().iterator(); i.hasNext(); ) {
            String pfSymbol = i.next();
            if (symbol.startsWith(pfSymbol)) {
                return (Prefix)_symbolToPrefix.get(pfSymbol);
            }
        }
        return null;
    }
    
    /**
     * Returns the prefix for the specified converter.
     *
     * @param converter the unit converter.
     * @return the corresponding prefix or <code>null</code> if none.
     */
    public Prefix getPrefix (UnitConverter converter) {
        return (Prefix)_converterToPrefix.get(converter);
    }
    
    /**
     * Returns the symbol for the specified prefix.
     *
     * @param prefix the prefix.
     * @return the corresponding symbol or <code>null</code> if none.
     */
    public String getSymbol (Prefix prefix) {
        return _prefixToSymbol.get(prefix);
    }
}
