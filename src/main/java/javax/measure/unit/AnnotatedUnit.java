/*
 * JScience - Java Tools and Libraries for the Advancement of Sciences
 * Copyright (c) 2005-2009, JScience (http://jscience.org/)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package javax.measure.unit;

import javax.measure.converter.UnitConverter;
import javax.measure.quantity.Quantity;

/**
 * <p> This class represents an annotated unit. It  allows for unit specialization
 *     and annotation without changing the unit semantic. For example:[code]
 *        public class Size extends Measurable<Length> {
 *             private double meters;
 *             ...
 *             public static class Unit extends AnnotatedUnit<Length> {
 *                  private Unit(javax.measure.unit.Unit<Length> realUnit, String annotation) {
 *                      super(realUnit, annotation);
 *                  }
 *                  public static Size.Unit METER = new Size.Unit(SI.METER, "SIZE"); // Equivalent to SI.METER
 *                  public static Size.Unit INCH = new Size.Unit(NonSI.INCH, "SIZE"); // Equivalent to NonSI.INCH
 *             }
 *        }[/code]</p>
 * <p> Annotation are often written between curly braces behind units 
 *     but they do not change, for example "%{vol}", "kg{total}", or
 *     "{RBC}" (for "red blood cells") are equivalent to "%", "kg", and "1"
 *      respectively.</p>
 *       
 * @author  <a href="mailto:jean-marie@dautelle.com">Jean-Marie Dautelle</a>
 * @version 1.0, April 15, 2009
 */
public class AnnotatedUnit<Q extends Quantity> extends DerivedUnit<Q> {

    /**
     * Holds the annotation.
     */
    private final String _annotation;

    /**
     * Holds the real unit.
     */
    private final Unit<Q> _realUnit;

    /**
     * Creates an annotated unit for the specified unit.
     *
     * @param realUnit the real unit.
     * @param annotation the annotation.
     */
    public AnnotatedUnit(Unit<Q> realUnit, String annotation) {
        _realUnit = (realUnit instanceof AnnotatedUnit)
                ? ((AnnotatedUnit<Q>) realUnit).getRealUnit() : realUnit;
        _annotation = annotation;
    }

    /**
     * Returns the annotation of this unit.
     *
     * @return the annotation of this unit.
     */
    public String getAnnotation() {
        return _annotation;
    }

    /**
     * Returns the equivalent non-annotated unit.
     *
     * @return the real unit.
     */
    public final Unit<Q> getRealUnit() {
        return _realUnit;
    }

    @Override
    public final Unit<Q> toSI() {
        return _realUnit.toSI();
    }

    @Override
    public final UnitConverter getConverterTo(Unit<Q> unit) {
        return _realUnit.getConverterTo(unit);
    }

    @Override
    public boolean equals(Object that) {
        if (this == that)
            return true;
        if (!(that instanceof AnnotatedUnit))
            return false;
        AnnotatedUnit<?> thatUnit = (AnnotatedUnit<?>) that;
        return this._realUnit.equals(thatUnit._realUnit) &&
                this._annotation.equals(thatUnit._annotation);
    }

    @Override
    public int hashCode() {
        return _realUnit.hashCode() + _annotation.hashCode();
    }
    
    private static final long serialVersionUID = 1L;

}