/*
 * JScience - Java Tools and Libraries for the Advancement of Sciences
 * Copyright (c) 2005-2009, JScience (http://jscience.org/)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package javax.measure.unit;

import javax.measure.converter.UnitConverter;
import javax.measure.quantity.Quantity;

/**
 * <p> This class represents the multi-radix units (such as "hour:min:sec").
 *     Instances of this class are created using the {@link Unit#compound
 *     Unit.compound} method. Instances of this class are used mostly for
 *     {@link javax.measure.MeasureFormat formatting} purpose.</p>
 *
 * <p> Examples of compound units:[code]
 *     Unit<Duration> HOUR_MINUTE_SECOND = HOUR.compound(MINUTE).compound(SECOND);
 *     Unit<Angle> DEGREE_MINUTE_ANGLE = DEGREE_ANGLE.compound(MINUTE_ANGLE);
 *     Unit<Length> FOOT_INCH = FOOT.compound(INCH);
 *     [/code]</p>
 *
 * @author  <a href="mailto:jean-marie@dautelle.com">Jean-Marie Dautelle</a>
 * @version 1.0, April 15, 2009
 */
public final class CompoundUnit<Q extends Quantity> extends DerivedUnit<Q> {

    /**
     * Holds the lowest (main) unit.
     */
    private final Unit<Q> _low;

    /**
     * Holds the highest unit(s).
     */
    private final Unit<Q> _high;

    /**
     * Creates a compound unit from the specified units.
     *
     * @param  high the highest unit(s).
     * @param  low the lowest unit(s).
     * @throws IllegalArgumentException if both units are not of same type
     *         (<code>!high.toSI().equals(low.toSI())</code>).
     */
    CompoundUnit(Unit<Q> high, Unit<Q> low) {
        if (!high.toSI().equals(low.toSI()))
            throw new IllegalArgumentException(
                    "Cannot compound " + high + " with " + low);
        if (low instanceof CompoundUnit) {
            _high = high.compound(((CompoundUnit<Q>) low).getHigh());
            _low = ((CompoundUnit<Q>) low).getLow();
        } else {
            _high = high;
            _low = low;
        }
    }

    /**
     * Returns the lowest unit or main unit of this compound unit
     * (never a {@link CompoundUnit}).
     *
     * @return the lower unit.
     */
    public Unit<Q> getLow() {
        return _low;
    }

    /**
     * Returns the high unit(s) of this compound unit (can be a
     * {@link CompoundUnit} itself).
     *
     * @return the high unit(s).
     */
    public Unit<Q> getHigh() {
        return _high;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that)
            return true;
        if (!(that instanceof CompoundUnit))
            return false;
        CompoundUnit<?> thatUnit = (CompoundUnit<?>) that;
        return this._low.equals(thatUnit._low) && this._high.equals(thatUnit._high);
    }

    @Override
    public int hashCode() {
        return _high.hashCode() ^ _low.hashCode();
    }

    /**
     * Overrides the {@link Unit#toString() default implementation} as
     * compound units are not recognized by the standard UCUM format.
     *
     * @return the textual representation of this compound unit.
     */
    @Override
    public String toString() {
        return _high + ":" + _low;
    }

    @Override
    public Unit<Q> toSI() {
        return _low.toSI();
    }

    @Override
    public final UnitConverter getConverterTo(Unit<Q> unit) {
        return _low.getConverterTo(unit);
    }
    
    private static final long serialVersionUID = 1L;

}