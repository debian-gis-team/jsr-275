/*
 * JScience - Java Tools and Libraries for the Advancement of Sciences
 * Copyright (c) 2005-2009, JScience (http://jscience.org/)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package javax.measure.unit;

import javax.measure.converter.UnitConverter;
import javax.measure.quantity.Quantity;

/**
 * <p> This class represents the building blocks on top of which all others
 *     units are created. Base units are typically dimensionally independent.
 *     Although, the actual unit dimension is determinate by the current
 *     {@link Dimension.Model model}. Using the {@link
 *     Dimension.Model#STANDARD standard} model, all seven SI base units 
 *     are dimensionally independent.</p>
 *
 * <p> This class defines the "standard base units" which includes SI base
 *     units and possibly others user-defined base units. It does not represent 
 *     the base units of a specific {@link SystemOfUnits}.</p>
 *           
 * @author  <a href="mailto:jean-marie@dautelle.com">Jean-Marie Dautelle</a>
 * @version 1.0, April 15, 2009
 * @see <a href="http://en.wikipedia.org/wiki/SI_base_unit">
 *       Wikipedia: SI base unit</a>
 */
public class BaseUnit<Q extends Quantity> extends Unit<Q> {

    /**
     * Holds the symbol.
     */
    private final String _symbol;

    /**
     * Creates a base unit having the specified symbol. 
     *
     * @param symbol the symbol of this base unit.
     * @throws IllegalArgumentException if the specified symbol is 
     *         associated to a different unit.
     */
    public BaseUnit(String symbol) {
        _symbol = symbol;
        // Checks if the symbol is associated to a different unit.
        synchronized (Unit.SYMBOL_TO_UNIT) {
            Unit<?> unit = Unit.SYMBOL_TO_UNIT.get(symbol);
            if (unit == null) {
                Unit.SYMBOL_TO_UNIT.put(symbol, this);
                return;
            }
            if (!(unit instanceof BaseUnit))
                throw new IllegalArgumentException("Symbol " + symbol + " is associated to a different unit");
        }
    }

    /**
     * Returns the unique symbol for this base unit. 
     *
     * @return this base unit symbol.
     */
    public final String getSymbol() {
        return _symbol;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that)
            return true;
        if (!(that instanceof BaseUnit))
            return false;
        BaseUnit<?> thatUnit = (BaseUnit<?>) that;
        return this._symbol.equals(thatUnit._symbol);
    }

    @Override
    public int hashCode() {
        return _symbol.hashCode();
    }

    @Override
    public Unit<Q> toSI() {
        return this;
    }

    @Override
    public final UnitConverter getConverterTo(Unit<Q> unit) {
        return unit.equals(this) ? UnitConverter.IDENTITY : super.getConverterTo(unit);
    }
    private static final long serialVersionUID = 1L;

}