/*
 * JScience - Java Tools and Libraries for the Advancement of Sciences
 * Copyright (c) 2005-2009, JScience (http://jscience.org/)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package javax.measure.unit;

import javax.measure.unit.format.*;
import java.io.IOException;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.Locale;

/**
 * <p> This class provides the interface for formatting and parsing {@link 
 *     Unit units}.</p>
 *     
 * <p> For all {@link SI} units, the 20 SI prefixes used to form decimal
 *     multiples and sub-multiples of SI units are recognized.
 *     {@link NonSI} units are directly recognized. For example:[code]
 *        Unit.valueOf("m°C").equals(SI.MILLI(SI.CELSIUS))
 *        Unit.valueOf("kW").equals(SI.KILO(SI.WATT))
 *        Unit.valueOf("ft").equals(SI.METRE.multiply(3048).divide(10000))[/code]</p>
 *
 * @author <a href="mailto:jean-marie@dautelle.com">Jean-Marie Dautelle</a>
 * @author Eric Russell
 * @version 1.0, April 15, 2009
 */
public abstract class UnitFormat extends Format {

    /**
     * Returns the unit format for the default locale.
     * 
     * @return {@link LocalFormat#getInstance()}
     */
    public static UnitFormat getInstance() {
        return LocalFormat.getInstance();
    }

    /**
     * Returns the unit format for the specified locale.
     *
     * @param locale the locale for which the format is returned.
     * @return {@link LocalFormat#getInstance(java.util.Locale)}
     */
    public static UnitFormat getInstance(Locale locale) {
        return LocalFormat.getInstance(locale);
    }

    /**
     * Returns the standard
     * <a href="http://unitsofmeasure.org/">UCUM </a> unit format.
     * This is the format used by {@link Unit#valueOf(CharSequence)
     * Unit.valueOf(CharSequence)} and {@link Unit#toString() Unit.toString()}).
     *
     * <p> This format uses characters range <code>0000-007F</code> exclusively
     * and <b>is not</b> locale-sensitive. For example: <code>kg.m/s2</code></p>
     *
     * <p>For a description of the UCUM format including BNF, see:
     *     <a href="http://aurora.regenstrief.org/~ucum/ucum.html">
     * The Unified Code for Units of Measure</a></p>
     * 
     * @return {@link UCUMFormat#getCaseSensitiveInstance()}
     */
    public static UnitFormat getStandard() {
        return UCUMFormat.getCaseSensitiveInstance();
    }

    /**
     * Base constructor.
     */
    protected UnitFormat() {
    }

    /**
     * Formats the specified unit.
     *
     * @param unit the unit to format.
     * @param appendable the appendable destination.
     * @throws IOException if an error occurs.
     */
    public abstract Appendable format(Unit<?> unit, Appendable appendable)
            throws IOException;

    /**
     * Parses a portion of the specified <code>CharSequence</code> from the
     * specified position to produce a unit. If there is no unit to parse 
     * {@link Unit#ONE} is returned.
     *
     * @param csq the <code>CharSequence</code> to parse.
     * @param cursor the cursor holding the current parsing index.
     * @return the unit parsed from the specified character sub-sequence.
     * @throws IllegalArgumentException if any problem occurs while parsing the
     *         specified character sequence (e.g. illegal syntax).
     */
    public abstract Unit<?> parse(CharSequence csq, ParsePosition cursor)
            throws IllegalArgumentException;

    @Override
    public final StringBuffer format(Object obj, final StringBuffer toAppendTo,
            FieldPosition pos) {
        if (!(obj instanceof Unit))
            throw new IllegalArgumentException("obj: Not an instance of Unit");
        if ((toAppendTo == null) || (pos == null))
            throw new NullPointerException(); // Format contract.
        try {
            return (StringBuffer) format((Unit<?>) obj, (Appendable) toAppendTo);
        } catch (IOException ex) {
            throw new Error(ex); // Cannot happen.
        }
    }

    @Override
    public final Unit<?> parseObject(String source, ParsePosition pos) {
        try {
            return parse(source, pos);
        } catch (IllegalArgumentException e) {
            return null; // Unfortunately the message why the parsing failed
        }                // is lost; but we have to follow the Format spec.

    }

    /**
     * Convenience method equivalent to {@link #format(Unit, Appendable)}
     * except it does not raise an IOException.
     *
     * @param unit the unit to format.
     * @param dest the appendable destination.
     * @return the specified <code>StringBuilder</code>.
     */
    public final StringBuilder format(Unit<?> unit, StringBuilder dest) {
        try {
            return (StringBuilder) this.format(unit, (Appendable) dest);
        } catch (IOException ex) {
            throw new Error(ex); // Can never happen.
        }
    }
    
    private static final long serialVersionUID = 1L;

}